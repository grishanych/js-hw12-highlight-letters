let buttons = document.querySelectorAll('.btn');
document.addEventListener('keydown', (event) => {
    for(let btn of buttons){
        btn.classList.remove('active')
        if(event.code === btn.dataset.key || event.code.slice(3) === btn.textContent){
        btn.classList.add('active')
        }
    }
})